# Spring Boot with Kafka Producer Example

This Project covers how to use Spring Boot with Spring Kafka to Publish JSON/String message to a Kafka topic
## Start Zookeeper
- `bin/zookeeper-server-start.sh config/zookeeper.properties`

## Start Kafka Server
- `bin/kafka-server-start.sh config/server.properties`

## Create Kafka Topic
- `bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic Kafka_Order`

## Consume from the Kafka Topic via Console
- `bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic Kafka_Order --from-beginning`

## Publish message via WebService
- `http://localhost:8081/kafka/dou/publish/order`
```json
curl --location --request POST 'http://localhost:8081/kafka/dou/publish/order' \
   --header 'Content-Type: application/json' \
   --data-raw '{
       "testName": "Should send attributions",
       "orderType": "ONLINE",
       "expectedTotalRequestsProcessed": 1,
       "expectedReqsSendToAttributions": 1,
       "expectedOnlineOrders": 1,
       "expectedOfflineOrders": 0,
       "expectedTransactionsWithNoGuidKey": 0,
       "items": {
           "itemIdentifier": "test",
           "baseUpc": "0000000000001",
           "description": "Cereals"
       }
   }'
```
