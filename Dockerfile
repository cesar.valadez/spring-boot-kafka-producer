FROM openjdk:8-jdk-alpine

WORKDIR /app
EXPOSE 8080

COPY start.sh /app/
COPY . /app/
RUN chmod +x /app/start.sh
ENTRYPOINT["/app/kafka/zookeeper-server-start.sh"]



