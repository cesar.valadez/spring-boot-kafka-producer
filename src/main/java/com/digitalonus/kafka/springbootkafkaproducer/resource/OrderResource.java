package com.digitalonus.kafka.springbootkafkaproducer.resource;

import com.digitalonus.kafka.springbootkafkaproducer.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("kafka/dou")
public class OrderResource {

    @Autowired
    private KafkaTemplate<String, Order> kafkaTemplate;
    private static final String TOPIC_ORDER = "Kafka_Order_DOU2";

    @PostMapping(value = "/publish/order", consumes = "application/json")
    public String post(@RequestBody Order order) {
        kafkaTemplate.send(TOPIC_ORDER, order);
        return "Published";
    }

    @PostMapping(value = "/publish/item", consumes = "application/json")
    public String postItem(@RequestBody Order order) {
        kafkaTemplate.send(TOPIC_ORDER, order);
        return "Published";
    }

}
