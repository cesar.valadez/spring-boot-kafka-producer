package com.digitalonus.kafka.springbootkafkaproducer;

import com.digitalonus.kafka.utils.TestHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RunWith(Parameterized.class)
@EnableAutoConfiguration
@TestExecutionListeners(listeners = {
        DependencyInjectionTestExecutionListener.class,
        MockitoTestExecutionListener.class
})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SpringBootKafkaProducerApplication.class)
@EmbeddedKafka
public class SpringBootKafkaProducerApplicationTests {
    private static final String TOPIC = "local-test";
    private final User user;

    @ClassRule
    public static final SpringClassRule SPRING_CLASS_RULE = new SpringClassRule();

    @Rule
    public static final SpringMethodRule springMethodRule = new SpringMethodRule();

    @Autowired
    private EmbeddedKafkaBroker embeddedKafkaBroker;

    private Producer<String, String> producer;

    private ObjectMapper objectMapper;

    public SpringBootKafkaProducerApplicationTests(User user) {
        this.user = user;
    }

    @Parameterized.Parameters(name = "{0}")
    public static Collection<Object[]> data() {
        System.out.println("1");
        List<User> inputs = TestHelper.loadTestData("/integration/**");
        return inputs.stream().map(user -> new Object[]{user}).collect(Collectors.toList());
    }

    @Before
    public void setup() {
        System.out.println("2");

        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.CLOSE_CLOSEABLE.WRITE_DATES_AS_TIMESTAMPS, false);

        Map<String, Object> configs = new HashMap<>(KafkaTestUtils.producerProps(embeddedKafkaBroker));

        producer = new DefaultKafkaProducerFactory<>(configs, new StringSerializer(), new StringSerializer()).createProducer();
    }

    @After
    public void tearDown() {
        // Clear metrics after each test
        //micrometerHelper.clearMetrics();
        //Mockito.reset(this.adAttributionClient);
    }





}
