package com.digitalonus.kafka.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

public class TestHelper {

    public static List<User> loadTestData(final String inputFilesPath) {
        List<User> inputs = new ArrayList<>();
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.registerModule(new JavaTimeModule());
            objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

            PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();

            Resource[] resources = resolver.getResources(inputFilesPath);

            for (Resource resource : resources) {
                User testDataFromJson = objectMapper.readValue(resource.getFile(), User.class);
                inputs.add(testDataFromJson);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return inputs;
    }
}
